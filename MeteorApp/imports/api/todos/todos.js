import { Mongo } from 'meteor/mongo';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';

export const Todos = new Mongo.Collection('Todos');

// Deny all client-side updates since we will be using methods to manage this 
// collection
Todos.deny({
    insert() { return true; },
    update() { return true; },
    remove() { return true; },
});

Todos.schema = new SimpleSchema({
    userId: {
        type: String,
        autoValue: function () { 
            return this.userId 
        }
    },
    text: {
        type: String,
        max: 100,
    },
    createdAt: {
        type: Date,
        denyUpdate: true,
    },
    checked: {
        type: Boolean,
        defaultValue: false,
    },
});

Todos.attachSchema(Todos.schema);

Todos.publicFields = {
    text: 1,
    createdAt: 1,
    checked: 1,
};