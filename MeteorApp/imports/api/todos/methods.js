import { Meteor } from 'meteor/meteor';
import { _ } from 'meteor/underscore';
import { ValidatedMethod } from 'meteor/mdg:validated-method';
import { SimpleSchema } from 'meteor/aldeed:simple-schema';
import { DDPRateLimiter } from 'meteor/ddp-rate-limiter';

import { Todos } from './todos.js';

export const insert = new ValidatedMethod({
    name: 'todos.insert',
    validate: new SimpleSchema({
        text: { type: String },
    }).validator(),
    run({ text }) {
        if (!this.userId) {
            throw new Meteor.Error('todos.insert.accessDenied',
                'Cannot add todos being not logged in');
        }

        const todo = {
            text,
            checked: false,
            createdAt: new Date(),
            userId: this.userId
        };

        Todos.insert(todo);
    },
});

export const toggleCheckedStatus = new ValidatedMethod({
    name: 'todos.toggleCheckedStatus',
    validate: new SimpleSchema({
        todoId: { type: String },
    }).validator(),
    run({ todoId }) {
        const todo = Todos.findOne(todoId);
        
        let newCheckedStatus = !todo.checked;

        if (todo.userId !== this.userId) {
            throw new Meteor.Error('todos.setCheckedStatus.accessDenied',
                'Cannot edit checked status of todo that is not yours');
        }

        Todos.update(todoId, { $set: {
            checked: newCheckedStatus,
        } });
    },
});

export const updateText = new ValidatedMethod({
    name: 'todos.updateText',
    validate: new SimpleSchema({
        todoId: { type: String },
        newText: { type: String },
    }).validator(),
    run({ todoId, newText }) {
        // This is complex auth stuff - perhaps denormalizing a userId onto 
        // todos would be correct here?
        const todo = Todos.findOne(todoId);

        if (todo.userId !== this.userId) {
            throw new Meteor.Error('todos.updateText.accessDenied',
                'Cannot edit todos that is not yours');
        }

        Todos.update(todoId, {
            $set: { text: newText },
        });
    },
});

export const remove = new ValidatedMethod({
    name: 'todos.remove',
    validate: new SimpleSchema({
        todoId: { type: String },
    }).validator(),
    run({ todoId }) {
        const todo = Todos.findOne(todoId);

        if (todo.userId !== this.userId) {
            throw new Meteor.Error('todos.remove.accessDenied',
                'Cannot remove todos that are not yours');
        }

        Todos.remove(todoId);
    },
});

// Get list of all method names on Todos
const TODOS_METHODS = _.pluck([
    insert,
    toggleCheckedStatus,
    updateText,
    remove,
], 'name');

if (Meteor.isServer) {
    // Only allow 5 todos operations per connection per second
    DDPRateLimiter.addRule({
        name(name) {
            return _.contains(TODOS_METHODS, name);
        },

        // Rate limit per connection ID
        connectionId() { return true; },
    }, 5, 1000);
}