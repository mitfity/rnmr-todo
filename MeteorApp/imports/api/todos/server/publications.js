import { Meteor } from 'meteor/meteor';
import { Todos } from '../todos.js';

Meteor.publish('todos', function todos() {
    const userId = this.userId;

    if (!userId) {
        return this.ready();
    }

    return Todos.find({
        userId,
    }, {
        fields: Todos.publicFields,
    });
});