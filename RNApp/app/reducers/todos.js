import { actionTypes } from 'app/actions'

const todo = (state, action) => {
    switch (action.type) {
        case actionTypes.ADD_TODO:
            return {
                text: action.text,
                checked: false
            }
        case actionTypes.TOGGLE_TODO:
            if (state.todoId !== action.todoId) {
                return state
            }

            return Object.assign({}, state, {
                checked: !state.checked
            })

        default:
            return state
    }
}

const todos = (state = [], action) => {
    switch (action.type) {
        case actionTypes.ADD_TODO:
            return [
                ...state,
                todo(undefined, action)
            ]
        case actionTypes.TOGGLE_TODO:
            return state.map(t => todo(t, action))
        case actionTypes.TODOS_FETCHED:
            return action.todos
            
        default:
            return state
    }
}

export default todos