import { combineReducers } from 'redux'
import todos from './todos'
import visibilityFilter from './visibilityFilter'
import fetchStatus from './fetchStatus'

const todoApp = combineReducers({
    todos,
    visibilityFilter,
    fetchStatus
})

export default todoApp