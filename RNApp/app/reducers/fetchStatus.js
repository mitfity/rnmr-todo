import { actionTypes } from 'app/actions'

const fetchStatus = (state = {}, action) => {
    switch (action.type) {
        case actionTypes.LOAD_TODOS_REQUEST:
            return {
                todos: {
                    isFetching: true
                }
            }
        case actionTypes.LOAD_TODOS_FAILURE:
            return {
                todos: {
                    isFetching: false,
                    error: action.error
                }
            }
        case actionTypes.TODOS_FETCHED:
            return {
                todos: {
                    isFetching: false
                }
            }
            
        default:
            return state
    }
}

export default fetchStatus