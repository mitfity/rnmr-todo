import React from 'react';
import Meteor, { createContainer } from 'react-native-meteor';
import { Provider } from 'react-redux'
import { createStore, applyMiddleware } from 'redux'
import thunkMiddleware from 'redux-thunk'
    
import todoApp from './reducers'
import LoggedOut from './layouts/LoggedOut';
import LoggedIn from './layouts/LoggedIn';
import Loading from './components/Loading';
import config from './config';

Meteor.connect(config.METEOR_URL);
let store = createStore(
    todoApp, 
    applyMiddleware(
        thunkMiddleware
    )
);

const RNApp = (props) => {
  const { status, user, loggingIn } = props;

  if (status.connected === false || loggingIn) {
    return <Loading />;
  } else if (user !== null) {
    return (
        <Provider store={store}>
            <LoggedIn />
        </Provider>        
    );
  } else {
    return <LoggedOut />;
  }
};

RNApp.propTypes = {
  status: React.PropTypes.object,
  user: React.PropTypes.object,
  loggingIn: React.PropTypes.bool,
};

export default createContainer(() => {
  return {
    status: Meteor.status(),
    user: Meteor.user(),
    loggingIn: Meteor.loggingIn(),
  };
}, RNApp);
