import Meteor from 'react-native-meteor';
import actionTypes from './actionTypes'

export const loadTodos = () => {        
    return function (dispatch, getState) {
        let { todos } = getState()
        
        if (todos && todos.length) {
            return
        }

        dispatch({
            type: actionTypes.LOAD_TODOS_REQUEST
        })
        
        const TODOS_COLLECTION = 'Todos'
        const Todos = Meteor.collection(TODOS_COLLECTION)
        const todosSubscription = Meteor.subscribe('todos', {
            onReady: function () {
                dispatch({
                    type: actionTypes.TODOS_FETCHED,
                    todos: Todos.find()
                })
            },
            onStop: function(error) {
                dispatch({
                    type: actionTypes.LOAD_TODOS_FAILURE,
                    error
                })
            }
        })
        
        Meteor.ddp.on('updated', ({ methods }) => {
            dispatch({
                type: actionTypes.TODOS_FETCHED,
                todos: Todos.find()
            })          
        })
    }
}

export const addTodo = (text) => {
    return function (dispatch, getState) {
        Meteor.call('todos.insert', {
            text
        })
        
        return {
            type: actionTypes.ADD_TODO,
            text
        }
    }    
}

export const setVisibilityFilter = (filter) => {
    return {
        type: actionTypes.SET_VISIBILITY_FILTER,
        filter
    }
}

export const toggleTodo = (id) => {
    return function (dispatch, getState) {
        Meteor.call('todos.toggleCheckedStatus', {
            todoId: id            
        })        
        
        return {
            type: actionTypes.TOGGLE_TODO,
            id
        }
    }
}