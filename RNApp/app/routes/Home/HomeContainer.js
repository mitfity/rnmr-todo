import React from 'react';
import Home from './Home';
import Routes from '../';

const onTodoAppPress = (navigator) => {
  return navigator.push(Routes.getTodoAppRoute());
};

const HomeContainer = (props) => {
  return (
    <Home
      onTodoAppPress={() => onTodoAppPress(props.navigator)}
    />
  );
};

HomeContainer.propTypes = {
  navigator: React.PropTypes.object,
};

export default HomeContainer;
