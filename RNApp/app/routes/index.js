import React from 'react';
import Home from './Home';
import TodoApp from './TodoApp';
import Profile from './Profile';

const Routes = {
  getHomeRoute() {
    return {
      renderScene(navigator) {
        return <Home navigator={navigator} />;
      },

      getTitle() {
        return 'Home';
      },
    };
  },
  getTodoAppRoute() {
    return {
      renderScene(navigator) {
        return <TodoApp navigator={navigator} />;
      },

      getTitle() {
        return 'TodoApp';
      },
    };
  },
  getProfileRoute() {
    return {
      renderScene(navigator) {
        return <Profile navigator={navigator} />;
      },

      showNavigationBar: false,
    };
  },
};

export default Routes;
