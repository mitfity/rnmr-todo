import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux'
import { COLORS } from '../../styles';
import Footer from './Footer'
import AddTodo from './AddTodo'
import TodoList from './TodoList'
import { loadTodos } from 'app/actions'

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: COLORS.background,
    },
    main: {
        fontSize: 20,
        textAlign: 'center',
        color: COLORS.headerText,
        fontWeight: '400',
        fontStyle: 'italic',
    },
});

class TodoApp extends Component {
    componentWillMount() {
        this.props.dispatch(loadTodos());
    }
    
    render() {
        return (
            <View style={styles.container}>
                <AddTodo />
                <TodoList />
                <Footer />
            </View>
        );
    }
}

TodoApp = connect()(TodoApp)

export default TodoApp;
