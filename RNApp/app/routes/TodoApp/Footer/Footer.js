import React from 'react'
import { StyleSheet, View, Text } from 'react-native'
import FilterLink from '../FilterToggle'

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 20,
        padding: 10,
        flexDirection:'row', 
        flexWrap:'wrap'
    }
});

const Footer = () => (
    <View style={styles.container}>
        <Text>Show:</Text>
        <Text>{' '}</Text>
        <FilterLink filter="SHOW_ALL">
            All
        </FilterLink>
        <Text>{', '}</Text>
        <FilterLink filter="SHOW_ACTIVE">
            Active
        </FilterLink>
        <Text>{', '}</Text>
        <FilterLink filter="SHOW_CHECKED">
            Completed
        </FilterLink>
    </View>
)

export default Footer