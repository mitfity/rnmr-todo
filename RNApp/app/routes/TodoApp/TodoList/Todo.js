import React, { PropTypes } from 'react'
import { StyleSheet, Text, TouchableHighlight } from 'react-native'
import { COLORS } from 'app/styles'

const todoTextStyle = function (checked) {
    return {
        padding: 10,
        color: 'black',
        fontSize: 20,
        textDecorationLine: checked ? 'line-through' : 'none'
    };     
}

const Todo = ({ onPress, checked, text, id }) => (
    <TouchableHighlight
        underlayColor={COLORS.todoPress}
        key={id}
        onPress={onPress}>
        <Text
            style={todoTextStyle(checked)}>
            {text}
        </Text>
    </TouchableHighlight>
)

Todo.propTypes = {
    onPress: PropTypes.func.isRequired,
    checked: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired
}

export default Todo