import React, { PropTypes, Component } from 'react'
import { ListView } from 'react-native'
import Todo from './Todo'

class TodoList extends Component {
    componentWillMount() {
        this.dataSource = new ListView.DataSource({
            rowHasChanged: (row1, row2) => row1 !== row2
        });
    }
    
    render() {
        const { todos, onTodoPress } = this.props;
        let dataSource = this.dataSource.cloneWithRows(todos);
        
        return (
            <ListView      
                dataSource={dataSource}
                renderRow={(rowData, sectionID, rowID) =>
                    <Todo {...rowData}
                        onPress={() => onTodoPress(rowData._id)}
                    />
                }
            />
        );
    }
}

TodoList.propTypes = {
    todos: PropTypes.arrayOf(PropTypes.shape({
        _id: PropTypes.string.isRequired,
        checked: PropTypes.bool.isRequired,
        text: PropTypes.string.isRequired
    }).isRequired).isRequired,
    onTodoPress: PropTypes.func.isRequired
}

export default TodoList