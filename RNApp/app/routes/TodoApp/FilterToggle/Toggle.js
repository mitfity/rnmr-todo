import React, { PropTypes } from 'react'
import { StyleSheet, Text, TouchableHighlight } from 'react-native'

const styles = StyleSheet.create({
    active: {
        color: 'darkblue'
    }
});

const Toggle = ({ active, children, onPress }) => {
    if (active) {
      return <Text style={styles.active}>{children}</Text>
    }

    return (        
        <TouchableHighlight
            onPress={onPress}>
            <Text>
                {children}
            </Text>
        </TouchableHighlight>
    )
}

Toggle.propTypes = {
    active: PropTypes.bool.isRequired,
    children: PropTypes.node.isRequired,
    onPress: PropTypes.func.isRequired
}

export default Toggle