import { connect } from 'react-redux'
import { setVisibilityFilter } from 'app/actions'
import Toggle from './Toggle'

const mapStateToProps = (state, ownProps) => {
    return {
        active: ownProps.filter === state.visibilityFilter
    }
}

const mapDispatchToProps = (dispatch, ownProps) => {
    return {
        onPress: () => {
            dispatch(setVisibilityFilter(ownProps.filter))
        }
    }
}

const FilterToggle = connect(
    mapStateToProps,
    mapDispatchToProps
)(Toggle)

export default FilterToggle