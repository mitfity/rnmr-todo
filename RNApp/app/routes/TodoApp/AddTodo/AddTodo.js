import React, { Component } from 'react'
import { connect } from 'react-redux'
import { addTodo } from 'app/actions'
import tcomb from 'tcomb-form-native'
import { StyleSheet, Text, View, TouchableHighlight } from 'react-native'

const Form = tcomb.form.Form;

const TodoModel = tcomb.struct({
    text: tcomb.String,
});

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        marginTop: 50,
        padding: 20,
        backgroundColor: '#ffffff',
    },
    button: {
        height: 36,
        backgroundColor: '#48BBEC',
        borderColor: '#48BBEC',
        borderWidth: 1,
        borderRadius: 8,
        marginBottom: 10,
        alignSelf: 'stretch',
        justifyContent: 'center'
    },
    buttonText: {
        fontSize: 18,
        color: 'white',
        alignSelf: 'center'
    }
});

class AddTodo extends Component {
    onPress() {
        let { dispatch } = this.props;
        let value = this.refs.form.getValue();
        
        if (value) {
            dispatch(addTodo(value.text));
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <Form
                    ref="form"
                    type={TodoModel}
                />
                <TouchableHighlight
                    onPress={this.onPress.bind(this)} 
                    underlayColor='#99d9f4'
                    style={styles.button}>
                    <Text style={styles.buttonText}>
                        Add todo
                    </Text>
                </TouchableHighlight>
            </View>
        );
    }
}

AddTodo = connect()(AddTodo)

export default AddTodo